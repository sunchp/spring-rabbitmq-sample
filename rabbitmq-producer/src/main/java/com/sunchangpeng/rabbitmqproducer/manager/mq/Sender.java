package com.sunchangpeng.rabbitmqproducer.manager.mq;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Created by sunchangpeng
 */
@Component
public class Sender {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void send(String content) {
        rabbitTemplate.convertAndSend("hello", content);
    }
}
