package com.sunchangpeng.rabbitmqproducer.controller;

import com.sunchangpeng.rabbitmqproducer.manager.mq.Sender;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * Created by sunchangpeng
 */
@RestController
public class ProducerController {
    @Autowired
    private Sender sender;

    @GetMapping("/send")
    public String send(@RequestParam(defaultValue = "hello") String msg) {
        String context = msg +"  "+ new DateTime().toString("yyyy-MM-dd HH:mm:ss");
        System.out.println("Sender : " + context);

        sender.send(context);

        return "success";
    }
}
